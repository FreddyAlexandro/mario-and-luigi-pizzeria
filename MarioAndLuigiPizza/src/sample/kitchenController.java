package sample;


import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.image.ImageView;
import javafx.scene.shape.Shape;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class kitchenController implements Initializable{

    @FXML Label label;
    @FXML ProgressIndicator incador;
    @FXML ProgressIndicator incador2;
    @FXML ListView<Orden> lista;
    @FXML ListView<Orden> ordenesentrgadas;
    @FXML Shape oven;
    @FXML ImageView pizzaprogress;



    //Recursos
    private ObservableList<Orden> ordenObservableList = FXCollections.observableArrayList();
    static boolean running = true;
    Monitor monitor = new Monitor();


    Task<Void> task_Ordenes = new Task<Void>() {
        @Override
        protected Void call() throws Exception {
            int num = 1;
            while (running){
                monitor.makeOrder(num);
                Thread.sleep(1000);
                System.out.println(monitor.getOrdenObservableList().toString());
                //lista.setItems(monitor.getOrdenObservableList());
            }
            return null;
        }
    };

    Task<Void> task_Chef = new Task<Void>() {
        @Override
        protected Void call() throws Exception {
            while (running){

            }
            return null;
        }
    };



    Thread Marco = new Thread(task_Chef);
    Thread Luis = new Thread(task_Chef);
    Thread Cliente = new Thread(task_Ordenes);


    @FXML private void click(ActionEvent event){
        Cliente.start();
        //Marco.start();
        //Luis.start();

    }


    @FXML private void stopService(){
        running= false;
    }



    @Override
    public void initialize(URL arg, ResourceBundle rb) {
        Cliente.setDaemon(true);
    }
}

