package sample;

import java.util.Random;

/**
 * Created by Alex on 31/10/16.
 */
public class Orden extends Pizza {
    private int ordenN=0;
    private int Tamaño;
    private String[] ingredientes;

    public Orden(int n){
        this.ordenN = n;
        this.Tamaño = getTamaño();
        Random random = new Random();
        this.ingredientes = getIngredientesPizza(random.nextInt(3 - 1) + 1);

    }

    public Orden(){

    }

    public int getOrdenN() {

        return this.ordenN;
    }

    public int getTamañoPizza() {
        return Tamaño;
    }

    public String[] getIngredientes() {
        return ingredientes;
    }
}
