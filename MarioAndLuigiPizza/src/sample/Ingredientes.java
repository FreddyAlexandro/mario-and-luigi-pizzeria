package sample;

import java.util.Random;

/**
 * Created by Alex on 27/10/16.
 */
public class Ingredientes {
    public static final String MASA = "Masa";
    public static final String SALSA_TOMATE = "SALSA";
    public static final String QUESO_PARMESANO = "Queso Parmesano";

    public static final String PEPERONI = "Peperoni";
    public static final String JAMON = "Jamon";
    public static final String PIÑA = "Piña";
    public static final String TOCINO = "Tocino";
    public static final String CHAMPIÑONES = "Champiñones";
    public static final String PIMINETOS = "Pimientos";

    private static final String[] pizza1 = {
            MASA,
            SALSA_TOMATE,
            PEPERONI,
            QUESO_PARMESANO
    };

    private static final String[] pizza2 = {
            MASA,
            SALSA_TOMATE,
            JAMON,
            PIÑA,
            QUESO_PARMESANO

    };

    private static final String[] pizza3 = {
            MASA,
            SALSA_TOMATE,
            TOCINO,
            CHAMPIÑONES,
            PIMINETOS,
            QUESO_PARMESANO
    };

    public String[] getIngredientesPizza(int R){
        switch (R){
            case 1:
                return pizza1;
            case 2:
                return pizza2;
            case 3:
                return pizza3;
            default:
                return pizza1;
        }
    }
}
