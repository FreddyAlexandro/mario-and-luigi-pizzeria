package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex on 03/11/16.
 */
public class Monitor {

    private List<Orden> colaOrdenes = new ArrayList<>();
    private ObservableList<Orden> ordenObservableList = FXCollections.observableArrayList();


    /**
     * Se crea un objeto de tipo orden y se almacena en el arreglo " "
     */
    public void makeOrder(int Num) {
        System.out.println("Nueva Orden");
        this.colaOrdenes.add(new Orden(Num));
        updateObservavleList();
    }



    public List<Orden> getColaOrdenes() {
        return colaOrdenes;
    }

    /**
     * retorna un objeto de tipo orden
     */
    public synchronized Orden getOrder(){
        return colaOrdenes.remove(0);
    }
    
    private void updateObservavleList(){
        ordenObservableList.clear();
        for (Orden tmp: colaOrdenes) {
            ordenObservableList.add(tmp);
        }
    }

    public ObservableList<Orden> getOrdenObservableList() {
        return ordenObservableList;
    }
}
