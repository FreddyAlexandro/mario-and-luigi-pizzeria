package sample;

import java.util.Random;

/**
 * Created by Alex on 27/10/16.
 */
public class Pizza extends Ingredientes {

    public static final int CHICA = 10;
    public static final int MEDIANA = 20;
    public static final int GRANDE = 30;


    public int getTamaño(){
        Random random = new Random();
        return (random.nextInt(3 - 1) + 1)*10;
    }

}
